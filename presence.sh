#!/bin/bash
 
# ___ _____ _____ _____               ___        ___    
#|_ _| ____| ____| ____|  _   _ _ __ (_) \      / / \   
# | ||  _| |  _| |  _|   | | | | '_ \| |\ \ /\ / / _ \  
# | || |___| |___| |___  | |_| | | | | | \ V  V / ___ \ 
#|___|_____|_____|_____|  \__,_|_| |_|_|  \_/\_/_/   \_\

HOSTS=$(nmap -sP 192.168.1.0/24 | tail -n 1 | cut -d '(' -f2 | cut -d ' ' -f1)
((HOSTS=HOSTS-1)) 
#Afairoume 1 (h perissoteres) IP apo to teliko apotelesma gia tous sta8erous
#hosts pou yparxoun sto diktyo (Routers,servers,klp)

if [[ ${HOSTS} -eq 0 ]]; then
    echo "There are only some ghosts in the lab, sorry we're closed"
else
    echo "There are ${HOSTS} students in the lab, we're open for business!"
fi
